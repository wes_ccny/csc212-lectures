#include "vector.h"
#include <iostream>
using std::cin;
using std::cout;

/* TODO: make sure you understand why calling this function without
 * writing our own copy constructor will lead to errors (either a
 * segfault or double free). */
void vecbyvalue(vector R);

int main()
{
	vector V;
	int x;
	while (cin >> x) {
		V.push_back(x);
	}
	vecbyvalue(V);
	for (size_t i = 0; i < V.size(); i++) {
		printf("V[%lu] = %i\n",i,V[i]);
		/* NOTE: someone asked in chat about this printf and I forgot
		 * to explain.  Sorry!  You can check out "man 3 printf" and
		 * it will tell you all about it.  Also, you could accomplish
		 * the same with cout of course, but I find it more cumbersome. */
	}
	return 0;
}

/* NOTE: copy constructor will be used to initialize R to
 * be a copy of whatever was given for the parameter.
 * E.g., if we called vecbyvalue(W), then copy constructor
 * for R with parameter W would be called for us. */
void vecbyvalue(vector R)
{
	R.push_back(100);
	for (size_t i = 0; i < R.size(); i++) {
		printf("  R[%lu] = %i\n",i,R[i]);
	}
}

/* hint: the default copy constructor will do this:
 * R.data = V.data;
 * R.size = V.size;
 * R.capacity = V.capacity; */
