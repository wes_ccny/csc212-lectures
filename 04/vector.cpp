#include "vector.h"
/* NOTE: single quotes searches for the included file in the
 * local directory first, then system directories. */
#include <cassert>

/* set arbitrary initial capacity */
#define INITCAPACITY 8

vector::vector()
{
	/* job of constructor is to setup all member variables
	 * to some "sane" initial state. */
	msize = 0;
	capacity = INITCAPACITY;
	data = new int[capacity];
}

vector::vector(const vector& V)
{
	/* make a "real" copy of V: */
	msize = V.msize;
	capacity = msize;
	data = new int[capacity];
	for (size_t i = 0; i < msize; i++) {
		data[i] = V.data[i];
	}
}

vector::~vector()
{
	delete[] data;
}

/* make V[i] work for our vector: */
int& vector::operator[](size_t i)
{
	assert(i<msize);
	/* remember: if we compile with NDEBUG defined,
	 * the assert disappears (effectively) before it
	 * reaches the proper compiler. */
	return data[i];
	/* NOTE: because we are returning a reference (int&),
	 * we can read and write to V[i]!  If it were by value,
	 * then V[i] = 10 would not compile. */
}

size_t vector::size()
{
	return msize;
}

int vector::back()
{
	assert(msize>0);
	return data[msize-1];
}

/* add element to end of vector.
 * Time analysis: almost always (save a logarithmic
 * number of calls in the length) it is O(1).  However
 * if a resize is triggered, it requires O(n). */
void vector::push_back(int x)
{
	/* see if we need more space: */
	if (msize == capacity) {
		/* looks like we are out of space... */
		/* question: how much bigger to make it?
		 * If we expand by one, then what is the cost of
		 * doing push_back over and over n times?  Could
		 * be O(n^2)!  Better approach: double each time. */
		/* "right way": use realloc.
		 * "educational way": do it ourselves: */
		/* we'll just make a new array of twice the size
		 * and copy everything over. */
		int* A = new int[capacity*2];
		for (size_t i = 0; i < msize; i++)
			A[i] = data[i];
		delete[] data;
		data = A;
		capacity *= 2;
	}
	/* and now we can at last push_back... */
	data[msize++] = x;
}

/* remove last element: */
void vector::pop_back()
{
	assert(msize>0);
	msize--;
}



#if 0
int* A = malloc(10000000*sizeof(int));
... once done with A...
break;??
return;??
free(A);

/* NOTE: destructor is called automatically when a variable
 * goes out of scope! */
int somenonmainfunction()
{
	vector V; /* here, the constructor vector() is called */
	/* use V for whatever... */
	return 42;
} /* V goes out of scope; ~vector() gets called */

int main(int argc, char *argv[])
{
	V.push_back(10);
	return 0;
}
#endif
