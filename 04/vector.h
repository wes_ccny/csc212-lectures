/* our own vector (of integers) */
#pragma once /* prevents multiple copies of this file from
                being #included. */

#include <cstddef> /* for size_t */

class vector {
public:
	void push_back(int x);
	void pop_back();
	int back(); /* gives last element (V[V.size()-1]) */
	size_t size();
	int& operator[](size_t i);
	/* constructors and such -- important for ANY class that
	 * manages dynamic memory! */
	vector(); /* NOTE: name of constructor is same as that
	             of the class, and there is NO return type. */
	/* also need a *copy constructor*.  NOTE: the copy constructor
	 * *defines* what "by-value" means for your class!  Hence the
	 * following is NOT a good prototype (would be self-referencing):
	 * vector(vector V); */
	vector(const vector& V);
	~vector(); /* destructor; called when a vector goes out of scope */
	/* also need assignment operator (describes what
	 * happens on X=Y for vectors): */
	/* TODO: Try to write an assignment operator.
	 * Two things to look out for when you do:
	 * 1. (x = y) = z is a thing you can do with built in types in C/C++
	 *    and it will have the effect of setting x to z and leaving y alone.
	 * 2. Make sure your program doesn't blow up if someone does x = x; */
	#if 0
	vector& operator=(const vector& RHS);
	// X = (Y = Z);
	vector X,Y;
	X = Y;
	// would be the same as this:
	X.data = Y.data;
	X.size = Y.size;
	X.capacity = Y.capacity;
	#endif

private:
	int* data; /* pointer to array used for storage */
	size_t msize; /* number of elements */
	size_t capacity; /* size of array */
};
