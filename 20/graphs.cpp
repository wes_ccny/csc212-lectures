#include <cstdio>
#include <cassert>
#include <vector>
using std::vector;

/* we'll use an adjacency matrix to store our graphs: */
typedef vector<int> row;
typedef vector<row> matrix;

void readgraph(matrix& G)
{
	size_t n; /* number of vertexes */
	/* read graph from stdin: */
	G.clear();
	G.push_back(row());
	char c;
	while (fread(&c,1,1,stdin) && c != '\n') {
		G[0].push_back((c=='0')?0:1);
	}
	n = G[0].size();
	/* now read n-1 more rows of n elements: */
	G.resize(n);
	for (size_t i = 1; i < n; i++) {
		G[i].resize(n);
		size_t j = 0;
		while (fread(&c,1,1,stdin) && c != '\n') {
			G[i][j++] = (c=='0')?0:1;
		}
		assert(j == n);
	}
}

const char* nodefill  = "darkolivegreen3";
const char* bgcolor   = "black";
const char* edges     = "white";

void printgraph(const matrix& G, const char* fname)
{
	char dotcmd[512];
	snprintf(dotcmd,511,"dot -Tsvg -o '%s'",fname);
	FILE* fsvg = popen(dotcmd,"w");
	if (fsvg == 0) {
		fprintf(stderr, "couldn't subprocess \"%s\"\n",dotcmd);
		return;
	}
	fprintf(fsvg, "digraph G {\n");
	fprintf(fsvg, "  bgcolor=%s\n  edge [color=%s]\n",bgcolor,edges);
	fprintf(fsvg, "  node [style=filled color=%s fillcolor=%s shape=circle]\n",
			edges, nodefill);

	for (size_t i = 0; i < G.size(); i++) {
		for (size_t j = 0; j < G[i].size(); j++) {
			if (G[i][j]) fprintf(fsvg, "%lu -> %lu\n",i,j);
		}
	}
	fprintf(fsvg, "}\n");
	int status = pclose(fsvg);
	if (status == -1) {
		fprintf(stderr, "pclose failed.\n");
	}
}

void printgraph_u(const matrix& G, const char* fname)
{
	char dotcmd[512];
	snprintf(dotcmd,511,"dot -Tsvg -o '%s'",fname);
	FILE* fsvg = popen(dotcmd,"w");
	if (fsvg == 0) {
		fprintf(stderr, "couldn't subprocess \"%s\"\n",dotcmd);
		return;
	}
	fprintf(fsvg, "graph G {\n");
	fprintf(fsvg, "  bgcolor=%s\n  edge [color=%s]\n",bgcolor,edges);
	fprintf(fsvg, "  node [style=filled color=%s fillcolor=%s shape=circle]\n",
			edges, nodefill);

	for (size_t i = 0; i < G.size(); i++) {
		for (size_t j = 0; j < i; j++) {
			if (G[i][j]) fprintf(fsvg, "%lu -- %lu\n",i,j);
		}
	}
	fprintf(fsvg, "}\n");
	int status = pclose(fsvg);
	if (status == -1) {
		fprintf(stderr, "pclose failed.\n");
	}
}

/* depth first search.  Assumptions:
 * 1. vertexes are labeled 0,1,...,n-1 = |G|.
 * 2. before the first call, P has been setup so that all
 *    entries are -1 (to denote unvisited) and maybe set
 *    P[s] = -2?
 * */
void dfs(const matrix& G, int s, vector<int>& P)
{
	/* NOTE: G[s][v] tells you if there is an edge from s --> v. */
	for (size_t v = 0; v < G[s].size(); v++) {
		if (G[s][v] && P[v] < 0) { // unvisited neighbor
			P[v] = s;
			dfs(G,v,P);
		}
	}
}

/* TODO: re-write dfs from scratch */
/* TODO: re-write dfs using an explicit stack instead of recursion */
/* TODO: after calling dfs in main, use P to print the path that
 * was found to each reachable vertex. */

int main(void)
{
	matrix G;
	readgraph(G);
	printgraph(G,"/tmp/graph.svg");
	vector<int> P(G.size(),-1);
	P[0] = -2;
	dfs(G,0,P);
	return 0;
}
