/* simple C++ class for linked lists */
#pragma once /* make sure this isn't included more than once */

#include <cassert>
#include <cstddef>

/* node structure */
struct node {
	int data;
	node* next;
	/* constructor for convenience: */
	node(int d=0, node* n=NULL) : data(d), next(n) {}
};

class list {
public:
	/* constructors */
	list();
	list(const list& L);
	/* assignment */
	list& operator=(list RHS);
	/* destructor */
	~list();

	/* pointer-like thing to go through list.
	 * NOTE: nested classes are "friends" since
	 * C++11 (?) */
	class iterator {
	public:
		iterator() {p = NULL;};
		iterator(const list& L) {p = L.front;}
		iterator& operator++() { // prefix: ++i
			// advance internal pointer and return new value
			p = p->next;
			return *this;
		}
		iterator operator++(int) { // postfix: i++
			iterator r(*this);
			p = p->next;
			return r;
		}
		int& operator*() {
			assert(p != NULL);
			return p->data;
		}
		bool operator!=(const iterator& RHS) {
			return (p != RHS.p);
		}
	private:
		node* p;
	};

	iterator begin();
	iterator end();

	void push_front(int x);
	void push_back(int x);
	void pop_front();
	void pop_back();
private:
	node* front;
	node* back;
};
