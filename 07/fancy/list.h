/* fancy C++ class for linked lists: make a doubly linked
 * list using only one pointer per node. */
#pragma once

#include <cassert>
#include <cstddef>

/* node structure */
struct node {
	int data;
	node* nbrs; /* will contain prev xor next */
	/* constructor for convenience: */
	node(int d=0, node* n=NULL) : data(d), nbrs(n) {}
};

class list {
public:
	/* constructors */
	list();
	list(const list& L);
	/* assignment */
	list& operator=(list RHS);
	/* destructor */
	~list();

	/* pointer-like thing to go through list.
	 * NOTE: nested classes are "friends" since C++11 (?) */
	class iterator {
	public:
		iterator() { q = p = NULL; }
		iterator(const list& L) { q = NULL; p = L.front; }
		iterator& operator++();   /* ++i */
		iterator operator++(int); /* i++ */
		/* NOTE: above two are defined in list.cpp */
		/* TODO: without adding any member variables, make an operator--
		 * that would go backwards through the list.  */
		int& operator*() { assert(p != NULL); return p->data; }
		bool operator!=(const iterator& RHS) { return (p != RHS.p); }
		bool operator==(const iterator& RHS) { return (p == RHS.p); }
	private:
		node* q; /* node before p */
		node* p; /* current node */
	};
	/* TODO: try to also write a *reverse* iterator.  You should also add
	 * functions to list called rbegin() and rend() (the former should give
	 * you an iterator pointing at the last node).  The increment (++) should
	 * go to the previous node (towards the front).  */

	iterator begin();
	iterator end();

	void push_front(int x);
	void push_back(int x);
	void pop_front();
	void pop_back();
private:
	node* front;
	node* back;
};
