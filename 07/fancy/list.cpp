#include "list.h"
#include <algorithm>
using std::swap;

/* get a size_t from a pointer: */
// #define INT(p) reinterpret_cast<size_t>(p)
// #define PTR(x) reinterpret_cast<node*>(x)
/* p ^= q */
// #define XOREQ(p,q) p = PTR((INT(p)^INT(q)))

/* old school ways: */
#define INT(p) (size_t)(p)
#define PTR(x) (node*)(x)
/* p ^= q */
#define XOREQ(p,q) p = PTR((INT(p)^INT(q)))

list::list()
{
	front = back = NULL;
}

list::list(const list& L)
{
	/* TODO: try to write this! */
}

/* LHS = RHS; ===> LHS.operator=(RHS); */
list& list::operator=(list RHS)
{
	/* TODO: try to write this! */
	return *this;
}

list::~list()
{
	/* TODO: try to write this! */
}

list::iterator list::begin()
{
	return list::iterator(*this);
}
list::iterator list::end()
{
	return list::iterator();
}

list::iterator& list::iterator::operator++()
{
	/* next @ q XOR p->nbrs.  Then move q to p... */
	node* next = PTR(INT(q) ^ INT(p->nbrs));
	q = p;
	p = next;
	return *this;
}

list::iterator list::iterator::operator++(int)
{
	iterator r(*this);
	node* next = PTR(INT(q) ^ INT(p->nbrs));
	q = p;
	p = next;
	return r;
}

void list::push_front(int x)
{
	/* add new node with x at the beginning */
	node* oldf = front;
	front = new node(x,front);
	if (oldf) /* original nonempty */
		XOREQ(oldf->nbrs,front);
	else
		back = front;
}

void list::pop_front()
{
	/* 1. delete first node; move front pointer
	 * 2. new front->nbrs should be stripped of old front */
	assert(front != NULL);
	node* p = front;
	front = front->nbrs; /* assuming NULL is 0 as it is in C! */
	if (front == NULL)
		back = NULL;
	else
		XOREQ(front->nbrs,p);
	delete p; /* have to do this last; using p after delete is UB,
	             even though I've never seen the value of a pointer
				 change by invoking delete on it. */
}

void list::push_back(int x)
{
	/* TODO: try to write this! */
}

void list::pop_back()
{
	/* TODO: try to write this! */
}
