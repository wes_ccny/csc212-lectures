#include "list.h"
#include <iostream>
using std::cout;
using std::cin;

int main()
{
	int x;
	list L;
	while (cin >> x) {
		L.push_back(x);
	}

	for (list::iterator i = L.begin(); i != L.end(); i++)
		cout << *i << " ";
	cout << "\n";
	return 0;
}
