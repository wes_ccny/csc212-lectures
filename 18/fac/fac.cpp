#include <stdio.h>

/* NOTE: you can also specialize *values* in templates! */

template <int n>
int factorial()
{
	return n * factorial<n-1>();
}

template<>
int factorial<0>()
{
	return 1;
}

int main()
{
	printf("5! == %i\n",factorial<5>());
	/* NOTE: the compiler did all the work!  If you look inside
	 * the generated assembly, you will see the answer (120)
	 * appears as a literal value!  Btw, to get the assembly
	 * from gcc: g++ -S -fverbose-asm fac.cpp and you will see
	 * a new file fac.s with the corresponding assembly. */
	return 0;
}
