NOTE: this small example shows that the *compiler itself* is Turing complete.
By making use of template specializations, we can prepare an input as C++
source which will induce the compiler to perform arbitrary computations.  To
see this for yourself, compile with `g++ -S -fverbose-asm fac.cpp` and examine
the output `fac.s`.  You will see that the `printf` call's argument is 120, as
a literal value.  (Meaning that by the time compiling was done, the whole
computation was also done!)
