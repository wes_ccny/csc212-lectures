#include "foo.h"
#include "swap.hpp"

/* NOTE: you can look around in your object files with the nm command, e.g.,
 * $ nm foo.o
 * */

int main()
{
	int x = 7;
	int y = 8;
	double w = 8.0;
	double z = 9.0;
	swap(x,y);
	swap(w,z);
	lovely(x);
	return 0;
}
