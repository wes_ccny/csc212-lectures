#pragma once

/* NOTE: can't really generate assembly from a template!
 * (Won't even know the size of things...) */
template <typename T>
void swap(T& a, T& b)
{
	T t = a;
	a = b;  // movq? movb??  Call an operator=???
	b = t;
}

/* NOTE: to resolve this issue (template doesn't make
 * sense as a thing to compile on its own), usually
 * you will #include the entire template source into
 * whatever files you use it from, and the different
 * instantiations will be compiled as needed. */

/* you can also *specialize* templates! */
#if 0
template <>
void swap<int>(int& a, int& b)
{
	a ^= b; // a = a XOR b
	b ^= a;
	a ^= b;
}
#endif

/* NOTE: specializations like this will NOT be weak symbols, so
 * be careful: if this were #included in multiple files whose
 * corresponding objects were subsequently linked, you'd get
 * linker errors from the mulitple definitions of swap<int>(...).
 * You can try it out by switching the 0 to a 1 above.
 * */
