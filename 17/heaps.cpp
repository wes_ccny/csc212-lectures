/* heaps and heapsort */
#include <iostream>
using std::cin;
using std::cout;
#include <vector>
using std::vector;
#include <assert.h>

#define LEFT(i)   2*i+1
#define RIGHT(i)  2*(i+1)
#define PARENT(i) (i-1)/2

template <typename T>
void swap(T& a, T& b)
{
	T t = a;
	a = b;
	b = t;
}

/* for visualizing heaps: */
void heap2dot(const vector<int>& H, const char* fname, size_t officialsize=-1)
{
	FILE* f = fopen(fname,"wb");
	fprintf(f, "digraph H {\n");
	fprintf(f, "  graph [ordering=\"out\"];\n");
	fprintf(f, "  bgcolor=black\n  edge [color=white]\n");
	fprintf(f, "  node [style=filled color=white fillcolor=violet shape=circle]\n");
	if (H.size() == 0) {
		fprintf(f, "}\n");
		fclose(f);
		return;
	}
	/* print root */
	if (officialsize == (size_t)-1) officialsize = H.size();
	fprintf(f, "  \"%i\" [label=%i]\n", 0,H[0]);
	/* all subsequent nodes will have a parent. we will let the child
	 * print the arrow from the parent after printing itself.  Note that
	 * at most one node will have an odd number of children. */
	const char* style[2] = {"","[style=dashed]"};
	size_t i;
	// for (i = 1; i < H.size(); i++) {
	for (i = 1; i < officialsize; i++) {
		fprintf(f, "  \"%lu\" [label=%i]\n", i,H[i]);
		fprintf(f, "  \"%lu\" -> \"%lu\" %s\n",PARENT(i),i,style[i%2]);
	}
	for (; i < H.size(); i++) {
		fprintf(f, "  \"%lu\" [label=%i fillcolor=coral]\n", i,H[i]);
		fprintf(f, "  \"%lu\" -> \"%lu\" %s\n",PARENT(i),i,style[i%2]);
	}
	/* if size is even, add invisible last right child */
	if (H.size() % 2 == 0) {
		fprintf(f, "  \"balance\" [label=\"\",width=.1,style=invis]\n");
		fprintf(f, "  \"%lu\" -> \"balance\" [style=invis]\n",PARENT(i));
	}
	fprintf(f, "}\n");
	fclose(f);
}

/* function to insert new value x into heap */
void insert(vector<int>& H, int x)
{
	H.push_back(x);
	size_t i = H.size() - 1;
	while (i!=0 && H[i] > H[PARENT(i)]) {
		/* fix violation */
		swap(H[i],H[PARENT(i)]);
		i = PARENT(i);
	}
}

/* fix heap assuming *only* violation is at index i,
 * which might be smaller than one of is children.
 * Parameter n is the "actual size" of heap, which might
 * be different than H.size(). */
void fixheap(vector<int>& H, size_t i, size_t n)
{
	while (LEFT(i) < n) { /* make sure i not a leaf */
		/* compare value at i with max of children.  Swap
		 * with max child if necessary. */
		size_t maxchild = LEFT(i);
		/* see if right child exists and is larger: */
		if (RIGHT(i) < n && H[RIGHT(i)] > H[maxchild])
			maxchild = RIGHT(i);
		/* if maxchild's value is greater than i's,
		 * do a swap + move violation downwards */
		if (H[i] < H[maxchild]) {
			swap(H[i],H[maxchild]);
			i = maxchild;
		} else { /* no violations remain; all done */
			break;
		}
	}
}

/* construct heap from unordered array/vector */
void buildheap(vector<int>& V)
{
	size_t n = V.size();
	for (size_t i = PARENT(n-1); i != (size_t)(-1); i--) {
		fixheap(V,i,V.size());
	}
}

void heapsort(vector<int>& V)
{
	buildheap(V);
	heap2dot(V,"/tmp/heaps/h0.dot");
	char fname[256];
	size_t n = V.size();
	for (size_t i = n-1; i > 0; i--) {
		swap(V[0],V[i]); /* V[n-1] is in its right place */
		fixheap(V,0,i); /* use i as new "offical" size */
		snprintf(fname,256,"/tmp/heaps/h%lu.dot",n-i);
		heap2dot(V,fname,i);
	}
}

/* TODO: Try writing buildheap and heapsort from scratch. */

#if 0
NOTE: to test out heapsort (and print the heaps as you go), you
could do with something like this (assuming working directory
is this directory):
  $ mkdir /tmp/heaps
  $ make
  $ shuf -i 1-20 | ./test
  $ for f in /tmp/heaps/*.dot ; do dot -Tsvg -o ${f/dot/svg} $f ; done
  < and now flip through the images in /tmp/heaps/... >
#endif

int main()
{
	vector<int> V;
	int x;
	while (cin >> x) {
		V.push_back(x);
	}
	heapsort(V);
	for (size_t i = 0; i < V.size(); i++) {
		cout << V[i] << " ";
	}
	cout << "\n";
	return 0;
}
