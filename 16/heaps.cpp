/* heaps and heapsort */
#include <iostream>
using std::cin;
using std::cout;
#include <vector>
using std::vector;
#include <assert.h>

#define LEFT(i)   2*i+1
#define RIGHT(i)  2*(i+1)
#define PARENT(i) (i-1)/2

template <typename T>
void swap(T& a, T& b)
{
	T t = a;
	a = b;
	b = t;
}

/* function to insert new value x into heap */
void insert(vector<int>& H, int x)
{
	H.push_back(x);
	size_t i = H.size() - 1;
	while (i!=0 && H[i] > H[PARENT(i)]) {
		/* fix violation */
		swap(H[i],H[PARENT(i)]);
		i = PARENT(i);
	}
}

/* fix heap assuming *only* violation is at index i,
 * which might be smaller than one of is children. */
void fixheap(vector<int>& H, size_t i)
{
	size_t n = H.size();
	while (LEFT(i) < n) { /* make sure i not a leaf */
		/* compare value at i with max of children.  Swap
		 * with max child if necessary. */
		size_t maxchild = LEFT(i);
		/* see if right child exists and is larger: */
		if (RIGHT(i) < n && H[RIGHT(i)] > H[maxchild])
			maxchild = RIGHT(i);
		/* if maxchild's value is greater than i's,
		 * do a swap + move violation downwards */
		if (H[i] < H[maxchild]) {
			swap(H[i],H[maxchild]);
			i = maxchild;
		} else { /* no violations remain; all done */
			break;
		}
	}
}

/* TODO: see if you can write an "extractmax" function,
 * and maybe try your hand at heapsort!  We'll go over
 * those next time. */

int main()
{
	return 0;
}
