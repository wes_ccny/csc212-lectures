/* our own vector (of integers) */
#pragma once /* prevents multiple copies of this file from
                being #included. */

#include <cstddef> /* for size_t */

class vector {
public:
	void push_back(int x);
	void pop_back();
	int back(); /* gives last element (V[V.size()-1]) */
	size_t size();
	int& operator[](size_t i);
	/* constructors and such -- important for ANY class that
	 * manages dynamic memory! */
	vector(); /* NOTE: name of constructor is same as that
	             of the class, and there is NO return type. */
	/* also need a *copy constructor*.  NOTE: the copy constructor
	 * *defines* what "by-value" means for your class!  Hence the
	 * following is NOT a good prototype (would be self-referencing):
	 * vector(vector V); */
	vector(const vector& V);
	~vector(); /* destructor; called when a vector goes out of scope */
	/* also need assignment operator (describes what
	 * happens on X=Y for vectors): */
	// vector& operator=(const vector& RHS);
	vector& operator=(vector RHS);
	// (X = Y) = Z;  // would set...X to Z and leave Y alone
	/* TODO: maybe add a few more of the standard functions from
	 * the STL vector, like resize, reserve, "shrink_to_fit" and
	 * clear.  And of course implement them...and test them... */

private:
	int* data; /* pointer to array used for storage */
	size_t msize; /* number of elements */
	size_t capacity; /* size of array */
};
