#include "vector.h"
#include <iostream>
using std::cin;
using std::cout;

void vecbyvalue(vector R);
void usesassignment(vector& R);

int main()
{
	vector V;
	int x;
	while (cin >> x) {
		V.push_back(x);
	}
	// vecbyvalue(V);
	usesassignment(V);
	for (size_t i = 0; i < V.size(); i++) {
		printf("V[%lu] = %i\n",i,V[i]);
	}
	return 0;
}

/* NOTE: copy constructor will be used to initialize R to
 * be a copy of whatever was given for the parameter.
 * E.g., if we called vecbyvalue(W), then copy constructor
 * for R with parameter W would be called for us. */
void vecbyvalue(vector R)
{
	R.push_back(100);
	for (size_t i = 0; i < R.size(); i++) {
		printf("  R[%lu] = %i\n",i,R[i]);
	}
}

/* TODO: be sure you understand why the following would lead
 * to errors if we used the default assignment operator.  Same
 * for the above function with the default copy constructor. */
void usesassignment(vector& R)
{
	vector X;
	X.push_back(99);
	X = R; /* should trigger assignment operator */
	X.push_back(100);
}

/* hint: the default copy constructor will do this:
 * R.data = V.data;
 * R.size = V.size;
 * R.capacity = V.capacity; */
