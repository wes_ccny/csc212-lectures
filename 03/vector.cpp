#include "vector.h"
/* NOTE: single quotes searches for the included file in the
 * local directory first, then system directories. */

void vector::push_back(int x)
{
	/* see if we need more space: */
	if (size == capacity) {
		/* looks like we are out of space... */
		/* question: how much bigger to make it?
		 * If we expand by one, then what is the cost of
		 * doing push_back over and over n times?  Could
		 * be O(n^2)!  Better approach: double each time. */
		/* "right way": use realloc.  (see 'man 3 realloc'.)
		 * "educational way": do it ourselves.  We'll just make
		 * a new array of twice the size and copy everything over. */
		int* A = new int[capacity*2];
		for (size_t i = 0; i < size; i++)
			A[i] = data[i];
		delete[] data;
		data = A;
		capacity *= 2;
	}
	/* and now we can at last push_back... */
	data[size++] = x;
	/* TODO: read the brief analysis in my 103 notes about growing
	 * the array by doubling the size. */
}

/* TODO: Try to write a few of the other member functions.  Easy ones might
 * be pop_back, size, and operator[]. */

// vim:foldlevel=2
