/* our own vector (of integers) */
#pragma once /* prevents multiple copies of this file from
                being #included. */

#include <cstddef> /* for size_t */

class vector {
public:
	void push_back(int x);
	void pop_back();
	int back(); /* gives last element (V[V.size()-1]) */
	size_t size();
	int& operator[](size_t i);
	/* constructors and such -- important for ANY class that
	 * manages dynamic memory! */
	vector(); /* NOTE: name of constructor is same as that
	             of the class, and there is NO return type. */
	~vector();
	/* also need assignment operator (describes what
	 * happens on X=Y for vectors): */
	#if 0
	vector& operator=(const vector& RHS);
	// X = (Y = Z);
	vector X,Y;
	X = Y;
	// would be the same as this:
	X.data = Y.data;
	X.size = Y.size;
	X.capacity = Y.capacity;
	#endif

private:
	int* data; /* pointer to array used for storage */
	size_t size; /* number of elements */
	size_t capacity; /* size of array */
};

// vim:foldlevel=2
