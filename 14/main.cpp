#include "tree.h"
#include <iostream>
using std::cin;
using std::cout;

int main()
{
	tree T;
	int x;
	while (cin >> x)
		T.insert(x);
	cout << T.search(9) << "\n";
	return 0;
}
