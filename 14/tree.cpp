#include "tree.h"

/* TODO: write the copy constructor (we did all the work, but didn't
 * plug it in...) */
/* TODO: try to write remove from scratch (with or without the hacks
 * where we added pointers) */
/* TODO: finish the traversal functions, and see if you can think
 * of anything useful to do with them.  (You have to write a function
 * that fits the description of "nodeproc" and give it as a parameter.) */
/* TODO: Write a function that prints (to stdout, say) the parenthesis
 * corresponding to the shape of the tree, as shown in lecture. */

template <typename T>
void swap(T& a, T& b)
{
	T t = a;
	a = b;
	b = t;
}

tree::tree()
{
	root = NULL;
}

/* copying a subtree.  Input: root of subtree to copy.
 * Output: pointer to copied root. */
node* copyST(node* p)
{
	/* base case: empty tree */
	if (p == NULL) return NULL;
	/* now assume that this algo works for any tree
	 * of size n-1 or less (n==size of tree pointed to
	 * by p).  NOTE: p->left has size at most n-1, as
	 * does p->right.  So...copyST works on those! */
	return new node(p->data,copyST(p->left),copyST(p->right));
}

// A = B; --> A.operator=(B);
tree& tree::operator=(tree T)
{
	/* assuming a valid copy constructor... */
	swap(root,T.root);
	return *this;
} // T goes out of scope and cleans up our old data...

tree::~tree()
{
	/* TODO: write this.  You can use one of the traversal functions
	 * to get it done if you think abou it. */
}

bool searchST(node* p, int x)
{
	if (!p) return false; /* empty subtree */
	if (x < p->data) return searchST(p->left,x);
	if (x > p->data) return searchST(p->right,x);
	return true;
}
bool tree::search(int x)
{
	return searchST(root,x);
}


/* insert as leaf node */
void insertST(node*& p, int x)
{
	if (!p) {
		p = new node(x);
		return;
	}
	if (x < p->data) insertST(p->left,x);
	else if (x > p->data) insertST(p->right,x);
	/* otherwise x is a duplicate (==p->data) */
}
void tree::insert(int x)
{
	return insertST(root,x);
}

/* IDEA: easy cases: no children, actually empty, even
 * one child is "easy".  To resolve the difficult case of
 * two children, replace *value* with successor's value,
 * then remove the duplicate.  And of course we have to
 * search for x first... */
void removeST(node*& p, int x)
{
	/* NOTE: p will be a synonym for either:
	 * 1. parent's left pointer
	 * 2. parent's right pointer
	 * 3. root pointer */
	if (p == NULL) return;
	if (x < p->data) { // look in left subtree
		removeST(p->left,x);
		return;
	} else if (p->data < x) {
		removeST(p->right,x);
		return;
	}
	// at this point we know x == p->data, so we've found the node
	// to remove.  Check which case we are in:
	if (p->left == NULL || p->right == NULL) {
		/* add pointers together, counting on NULL being
		 * 0 so that it is the identity element say for xor */
		node* child = (node*)((size_t)p->left ^ (size_t)p->right);
		delete p;
		p = child;
	} else { // annoying case of two children...
		node* s = p->right; /* will store successor */
		while (s->left) s = s->left;
		p->data = s->data;
		removeST(p->right,s->data);
		/* this recursive call will NOT result in further
		 * calls -- it will be caught by the previous case. */
	}
}

void tree::remove(int x)
{
	removeST(root,x);
}

/* traversal stuff: */

// typedef map<int,string>::iterator map_i;

typedef void (*nodeproc)(node*&);
/* interpretation: "nodeproc is a pointer to a function
 * that accepts a node pointer by reference and returns void" */

/* IDEA: apply f to each node in the subtree rooted at p,
 * in-order. */
void inorder(node*& p, nodeproc f)
{
	if (p == NULL) return;
	inorder(p->left,f);
	f(p); /* "process" root of subtree */
	inorder(p->right,f);
}
