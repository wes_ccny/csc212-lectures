/* header file for simple demo BST (of integers) */
#pragma once
#include <stddef.h>

struct node {
	int data;
	node* left;
	node* right;
	// node* parent;
	node(int d=0, node* l=NULL, node* r=NULL) : data(d),left(l),right(r){}
};

class tree {
public:
	/* constructors, destructors, assignment: */
	tree(); /* default */
	tree(const tree& T); /* copy */
	tree& operator=(tree T);
	~tree();

	bool search(int x);
	void insert(int x);
	void remove(int x);
private:
	node* root;
};
