#include <vector>
// using std::vector;
#include "vector.hpp"
#include <iostream>
using std::cin;
using std::cout;
#include <algorithm>
using std::sort;
using std::swap;

/* fill vector with random-ish 32 bit values. */
void randVec(std::vector<uint32_t>& V, size_t n)
{
	srand48(time(0));
	for (size_t i = 0; i < n; i++) {
		V.push_back(lrand48());
	}
}

/* insertion sort (for use in bucket sort). */
template <typename T>
void insort(T* A, size_t n)
{
	size_t i,j;
	for (i = 1; i < n; i++) {
		T x = A[i];
		for (j = i; j != 0 && A[j-1] > x; j--) {
			A[j] = A[j-1];
		}
		A[j] = x;
	}
}

template <typename T>
struct node {
	T data;
	node<T>* next;
	node() {}
	node(T d, node<T>* n) : data(d),next(n) {}
};

/* Requirement: all A[i] are in the range [0,m).
 * Sorts A in expected O(n) time, under the further
 * assumption that A[i] are uniformly distributed
 * in [0,m). */
/* version 0: use stl vectors and std::sort on buckets */
template <typename T>
void bucketsort0(std::vector<T>& A, T m)
{
	size_t n = A.size();
	/* step 1: make buckets */
	std::vector<std::vector<T>> B(n); /* buckets; all empty. */
	/* step 2: toss values into buckets: */
	for (size_t i = 0; i < n; i++) {
		B[A[i]*n/m].push_back(A[i]);
	}
	/* steps 3+4: sort buckets; copy back to A */
	size_t k = 0; /* destination for copying back into A */
	for (size_t i = 0; i < n; i++) { /* buckets */
		// std::sort(B[i].begin(),B[i].end());
		insort(&B[i][0],B[i].size());
		for (size_t j = 0; j < B[i].size(); j++) {
			A[k++] = B[i][j];
		}
	}
}

/* version 1: use homebrew vectors and insertion sort */
template <typename T>
void bucketsort(std::vector<T>& A, T m)
{
	size_t n = A.size();
	/* step 1: make buckets */
	std::vector<vector<T>> B(n); /* buckets; n of them, all empty. */
	/* step 2: toss values into buckets: */
	for (size_t i = 0; i < n; i++) {
		B[A[i]*n/m].push_back(A[i]);
	}
	/* steps 3+4: sort buckets; copy back to A */
	size_t k = 0; /* destination for copying back into A */
	for (size_t i = 0; i < n; i++) { /* buckets */
		// std::sort(B[i].begin(),B[i].end());
		insort(&B[i][0],B[i].size());
		for (size_t j = 0; j < B[i].size(); j++) {
			A[k++] = B[i][j];
		}
	}
}

/* version 2: use linked lists for buckets; sort buckets
 * as we build them. */
template <typename T>
void bucketsort2(std::vector<T>& A, T m)
{
	size_t n = A.size();
	/* step 1: make buckets */
	// vector<vector<T>> B(n); /* buckets; n of them, all empty. */
	std::vector<node<T>*> B(n,NULL);
	/* step 2: toss values into buckets: */
	for (size_t i = 0; i < n; i++) {
		size_t l = A[i]*n/m;
		/* insert A[i] into list B[l] */
		node<T> faux;
		faux.next = B[l];
		node<T>* q = &faux;
		node<T>* p = B[l];
		while (p && p->data < A[i]) {
			q = p;
			p = p->next;
		}
		/* new value goes between q and p: */
		q->next = new node<T>(A[i],p);
		B[l] = faux.next;
	}
	/* copy back to A */
	size_t k = 0; /* destination for copying back into A */
	for (size_t i = 0; i < n; i++) { /* buckets */
		node<T>* p = B[i];
		while (p) {
			A[k++] = p->data;
			node<T>* q = p;
			p = p->next;
			delete q;
		}
	}
}

/* version 3: same as version 2 but perform all node
 * allocations and deallocations in a batch. */
template <typename T>
void bucketsort3(std::vector<T>& A, T m)
{
	size_t n = A.size();
	/* step 1: make buckets */
	// vector<vector<T>> B(n); /* buckets; n of them, all empty. */
	std::vector<node<T>*> B(n,NULL);
	/* allocate all nodes at once: */
	node<T>* newnodes = new node<T>[n];
	/* pointer to next unused node: */
	node<T>* newnode = newnodes;
	/* step 2: toss values into buckets: */
	for (size_t i = 0; i < n; i++) {
		size_t l = A[i]*n/m;
		/* insert A[i] into list B[l] */
		node<T> faux;
		faux.next = B[l];
		node<T>* q = &faux;
		node<T>* p = B[l];
		while (p && p->data < A[i]) {
			q = p;
			p = p->next;
		}
		/* new value goes between q and p: */
		// q->next = new node<T>(A[i],p);
		/* setup pre-allocated node and place in list: */
		newnode->data = A[i];
		newnode->next = p;
		q->next = (newnode++);
		B[l] = faux.next;
	}
	/* copy back to A */
	size_t k = 0; /* destination for copying back into A */
	for (size_t i = 0; i < n; i++) { /* buckets */
		node<T>* p = B[i];
		while (p) {
			A[k++] = p->data;
			p = p->next;
		}
	}
	delete [] newnodes;
}

/* version 4: same as version 3 but doesn't actually
 * sort anything.  However, should have a nice access
 * pattern for the cache. */
template <typename T>
void bucketsort_notreally(std::vector<T>& A, T m)
{
	size_t n = A.size();
	/* step 1: make buckets */
	// vector<vector<T>> B(n); /* buckets; n of them, all empty. */
	std::vector<node<T>*> B(n,NULL);
	/* allocate all nodes at once: */
	node<T>* newnodes = new node<T>[n];
	/* pointer to next unused node: */
	node<T>* newnode = newnodes;
	/* step 2: toss values into buckets: */
	for (size_t i = 0; i < n; i++) {
		size_t l = A[i]*n/m;
		/* insert A[i] into list B[l] */
		l = i/3;
		node<T> faux;
		faux.next = B[l];
		node<T>* q = &faux;
		node<T>* p = B[l];
		while (p && p->data < A[i]) {
			q = p;
			p = p->next;
		}
		/* new value goes between q and p: */
		// q->next = new node<T>(A[i],p);
		/* setup pre-allocated node and place in list: */
		newnode->data = A[i];
		newnode->next = p;
		q->next = (newnode++);
		B[l] = faux.next;
	}
	/* copy back to A */
	size_t k = 0; /* destination for copying back into A */
	for (size_t i = 0; i < n; i++) { /* buckets */
		node<T>* p = B[i];
		while (p) {
			A[k++] = p->data;
			p = p->next;
		}
	}
	delete [] newnodes;
}

/* sort A using the d-th digit in base 2^b of each integer as the
 * sort key.  Further, let's say that b divides the bit-length of
 * each integer.  */
template <typename T>
void countingsort(std::vector<T>& A, size_t d, size_t b)
{
	/* mask should be 000000's followed by b 1's: */
	T mask = (T)(-1)>>(sizeof(T)*8-b);
	/* first make a frequency table: */
	// vector<size_t> C(1<<b,0);
	std::vector<uint32_t> C(1<<b,0);
	// vector<uint32_t> C(1<<b,0);
	/* NOTE: you can save time if you know the arrays will have less
	 * than 4 billion elements. by using smaller integers for C. */
	for (size_t i = 0; i < A.size(); i++)
		C[(A[i]>>(d*b))&mask]++;
	/* now modify C so that C[i] == #values <= i in A: */
	for (size_t i = 1; i < C.size(); i++)
		C[i] += C[i-1];
	std::vector<T> R(A.size());
	/* now fill R with the sorted result: */
	for (size_t i = A.size()-1; i != (size_t)(-1); i--) {
		/* look in C[digit d of A[i]].  Place A[i]
		 * at this location in R and decrement C for
		 * next time. */
		#if 1
		R[--C[(A[i]>>(d*b))&mask]] = A[i];
        #else
		--C[(A[i]>>(d*b))&mask];
		R[i] = A[i];
		#endif
	}
	/* at this point result is in R */
	A = std::move(R);
	/* NOTE: std::move will sort of cast R to an r-value
	 * so that we can make use of the more efficient r-value
	 * assignment if it exists. */
}

template <typename T>
void radixsort(std::vector<T>& A)
{
	size_t bitsofbase = 16;
	size_t digits = (sizeof(T)*8)/bitsofbase;
	for (size_t i = 0; i < digits; i++)
		countingsort(A,i,bitsofbase);
	/* TODO: how would you modify this so it works even if number of
	 * bits of the base does not divide the machine's register size? */
}

template <typename T>
size_t partition(T* A, size_t n) {
	T x = A[0];
	size_t i = -1;
	size_t j = n;
	/* INVARIANTS: A[0..i] <= x <= A[j..n-1]
	 * Clearly satisfied for now, as both sets are empty.  Now move i to the
	 * right and j to the left, being careful to preserve the invariants. */
	while (true) {
		do i++; while (A[i] < x);
		do j--; while (A[j] > x);
		if (i < j) swap(A[i],A[j]);
		else return j+1;
	}
}

template <typename T>
void quicksort(T* A, size_t n)
{
	if (n < 2) return;
	size_t p = partition(A,n);
	quicksort(A,p);
	quicksort(A+p,n-p);
}

int main(void)
{
	std::vector<uint32_t> V;
	size_t n = 1<<23;
	randVec(V,n);
	std::vector<uint32_t> VV(V); /* copy for radix (or bucket) */
	#if 0
	vector<uint32_t> VQ(V); /* copy for quicksort */
	#endif
	clock_t s,e; /* start + end times */
	double d; /* difference in seconds */

	/* test std::sort */
	/* NOTE: std::sort uses a *hybrid* algorithm: it starts with quick sort,
	 * then moves to heap and insertion once the arrays are small.
	 * (still runs in \Omega(n \log n) time) */
	s = clock();
	std::sort(V.begin(),V.end());
	e = clock();
	d = (e-s)/((double)CLOCKS_PER_SEC);
	printf(" std::sort completed in %f sec.\n",d);

	/* TODO: if you make bucket sort outperform std::sort,
	 * let me know how! */
	#if 0
	/* test bucket sort: */
	s = clock();
	// bucketsort(VV,((uint32_t)1)<<31);
	// bucketsort0(VV,((uint32_t)1)<<31);
	// bucketsort2(VV,((uint32_t)1)<<31);
	// bucketsort3(VV,((uint32_t)1)<<31);
	bucketsort_notreally(VV,((uint32_t)1)<<31);
	/* NOTE: see "man lrand48" for the choice of m */
	e = clock();
	d = (e-s)/((double)CLOCKS_PER_SEC);
	printf("bucketsort completed in %f sec.\n",d);
    #else
	/* test radix sort: */
	s = clock();
	radixsort(VV);
	e = clock();
	d = (e-s)/((double)CLOCKS_PER_SEC);
	printf("radix sort completed in %f sec.\n",d);
	#endif

	#if 0
	/* test quick sort: */
	s = clock();
	quicksort(&VQ[0],VQ.size());
	e = clock();
	d = (e-s)/((double)CLOCKS_PER_SEC);
	printf("quick sort completed in %f sec.\n",d);
	#endif

	/* make sure they are both in order: */
	s = clock();
	int same = 1;
	for (size_t i = 0; i < V.size(); i++) {
		if (V[i] != VV[i]) {
			same = 0;
			break;
		}
	}
	e = clock();
	d = (e-s)/((double)CLOCKS_PER_SEC);
	const char* msg = "\0not ";
	printf("arrays are %sidentical; check took %f sec.\n", msg+(1-same),d);
	return 0;
}
