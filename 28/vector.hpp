/* our own vector (of integers) */
#pragma once /* prevents multiple copies of this file from
                being #included. */

#include <cstddef> /* for size_t */
#include <cassert>

template <typename T>
class vector {
public:
	void push_back(T x);
	void pop_back();
	T back(); /* gives last element (V[V.size()-1]) */
	size_t size();
	T& operator[](size_t i);
	/* constructors and such -- important for ANY class that
	 * manages dynamic memory! */
	vector(); /* NOTE: name of constructor is same as that
	             of the class, and there is NO return type. */
	/* also need a *copy constructor*.  NOTE: the copy constructor
	 * *defines* what "by-value" means for your class!  Hence the
	 * following is NOT a good prototype (would be self-referencing):
	 * vector(vector V); */
	vector(const vector<T>& V);
	~vector(); /* destructor; called when a vector goes out of scope */

private:
	T* data; /* pointer to array used for storage */
	size_t msize; /* number of elements */
	size_t capacity; /* size of array */
};

/* set arbitrary initial capacity */
#define INITCAPACITY 4

template <typename T>
vector<T>::vector()
{
	/* job of constructor is to setup all member variables
	 * to some "sane" initial state. */
	msize = 0;
	capacity = INITCAPACITY;
	data = new T[capacity];
}

template <typename T>
vector<T>::vector(const vector<T>& V)
{
	/* make a "real" copy of V: */
	msize = V.msize;
	capacity = msize;
	data = new T[capacity];
	for (size_t i = 0; i < msize; i++) {
		data[i] = V.data[i];
	}
}

template <typename T>
vector<T>::~vector()
{
	delete[] data;
}

/* make V[i] work for our vector: */
template <typename T>
T& vector<T>::operator[](size_t i)
{
	assert(i<msize);
	/* remember: if we compile with NDEBUG defined,
	 * the assert disappears (effectively) before it
	 * reaches the proper compiler. */
	return data[i];
	/* NOTE: because we are returning a reference (int&),
	 * we can read and write to V[i]!  If it were by value,
	 * then V[i] = 10 would not compile. */
}

template <typename T>
size_t vector<T>::size()
{
	return msize;
}

template <typename T>
T vector<T>::back()
{
	assert(msize>0);
	return data[msize-1];
}

/* add element to end of vector.
 * Time analysis: almost always (save a logarithmic
 * number of calls in the length) it is O(1).  However
 * if a resize is triggered, it requires O(n). */
template <typename T>
void vector<T>::push_back(T x)
{
	/* see if we need more space: */
	if (msize == capacity) {
		T* A = new T[capacity*2];
		for (size_t i = 0; i < msize; i++)
			A[i] = data[i];
		delete[] data;
		data = A;
		capacity *= 2;
	}
	/* and now we can at last push_back... */
	data[msize++] = x;
}

/* remove last element: */
template <typename T>
void vector<T>::pop_back()
{
	assert(msize>0);
	msize--;
}
