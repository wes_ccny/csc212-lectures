#include "list.h"
#include <algorithm>
using std::swap;

list::list()
{
	front = back = NULL;
}

list::list(const list& L)
{
	/* copy all nodes... */
	if (L.front == NULL) {
		front = back = NULL;
		return;
	}
	// we can now assume L is not empty
	/* copy first node: */
	front = new node(L.front->data);
	back = front;
	node* p = L.front->next;
	/* now copy the rest */
	while (p) { /* while we have more to copy */
		back->next = new node(p->data);
		/* now that we've copied a node, p and
		 * back are "out of date". */
		back = back->next;
		p = p->next;
		/* loop invariant now maintained. */
	}
}

/* LHS = RHS; ===> LHS.operator=(RHS); */
list& list::operator=(list RHS)
{
	/* swap front pointer, copy back pointer...and profit? */
	swap(front,RHS.front);
	back = RHS.back;
	return *this; /* *this === LHS */
}

list::~list()
{
	/* need to deallocate ALL nodes!  IDEA: repeatedly
	 * delete first node. */
	while (front) {
		/* step 1: add pointer to first node: */
		node* p = front;
		/* step 2: advance front pointer */
		front = front->next;
		/* step 3: delete first node */
		delete p;
	}
	/* NOTE: back pointer will now be invalid, but no one
	 * will ever use it.  */
	// back = NULL;
}

void list::push_front(int x)
{
	/* add new node with x at the beginning */
	front = new node(x,front);
	/* might have to redirect back pointer too (if
	 * the list was empty for this call): */
	if (back == NULL) back = front;
}

void list::pop_front()
{
	assert(front != NULL);
	node* p = front;
	front = front->next;
	delete p;
	/* we might have just deleted back... */
	if (front == NULL) back = NULL;
}

void list::push_back(int x)
{
	if (back == NULL) { /* empty list */
		front = back = new node(x);
		return;
	}
	/* general case (non-empty) */
	back->next = new node(x);
	back = back->next;
}

/* TODO: try to write pop_back() */
/* TODO: add a function called "print" or similar that just
 * writes the list contents to stdout.  Then write a main.cpp
 * with some test code and see if we made any obvious mistakes.
 * NOTE: I have added a makefile, but it won't work until you
 * add a main.cpp or similar which contains a main() function.
 * (You will for sure get linker errors if you try to compile now.)
 * */
/* TODO: maybe read a bit about iterators in C++ (I think we'll try
 * to add them to our list class next time).
 * https://en.wikipedia.org/wiki/Iterator
 * */
