/* simple C++ class for linked lists */
#pragma once /* make sure this isn't included more than once */

#include <cassert>
#include <cstddef>

/* node structure */
struct node {
	int data;
	node* next;
	/* constructor for convenience: */
	node(int d=0, node* n=NULL) : data(d), next(n) {
		/* NOTE: doing it this way might invoke the default
		 * constructor for data and next, followed then by
		 * assignment.  Using the colon stuff ":" will just
		 * invoke the copy constructor. */
		// data = d;
		// next = n;
	}
};

/* list class: "wrapper" for node */
/* NOTE: only difference between struct and class is that
 * the members default to public in a struct and private
 * in a class. */
class list {
public:
	/* constructors */
	list();
	list(const list& L);
	/* assignment */
	list& operator=(list RHS);
	/* destructor */
	~list();

	void push_front(int x);
	void push_back(int x);
	void pop_front();
	void pop_back();
private:
	node* front;
	node* back;
};
