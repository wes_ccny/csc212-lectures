#include "tree.h"
#include <cstdio>

template <typename T>
void swap(T& a, T& b)
{
	T t = a;
	a = b;
	b = t;
}

tree::tree()
{
	root = NULL;
}

/* copying a subtree.  Input: root of subtree to copy.
 * Output: pointer to copied root. */
node* copyST(node* p)
{
	/* base case: empty tree */
	if (p == NULL) return NULL;
	/* now assume that this algo works for any tree
	 * of size n-1 or less (n==size of tree pointed to
	 * by p).  NOTE: p->left has size at most n-1, as
	 * does p->right.  So...copyST works on those! */
	return new node(p->data,copyST(p->left),copyST(p->right));
}

// A = B; --> A.operator=(B);
tree& tree::operator=(tree T)
{
	/* assuming a valid copy constructor... */
	swap(root,T.root);
	return *this;
} // T goes out of scope and cleans up our old data...

bool searchST(node* p, int x)
{
	if (!p) return false; /* empty subtree */
	if (x < p->data) return searchST(p->left,x);
	if (x > p->data) return searchST(p->right,x);
	return true;
}
bool tree::search(int x)
{
	return searchST(root,x);
}

/* insert as leaf node */
void insertST(node*& p, int x)
{
	if (!p) {
		p = new node(x);
		return;
	}
	if (x < p->data) insertST(p->left,x);
	else if (x > p->data) insertST(p->right,x);
	/* otherwise x is a duplicate (==p->data) */
}
void tree::insert(int x)
{
	return insertST(root,x);
}

/* IDEA: easy cases: no children, actually empty, even
 * one child is "easy".  To resolve the difficult case of
 * two children, replace *value* with successor's value,
 * then remove the duplicate.  And of course we have to
 * search for x first... */
void removeST(node*& p, int x)
{
	/* NOTE: p will be a synonym for either:
	 * 1. parent's left pointer
	 * 2. parent's right pointer
	 * 3. root pointer */
	if (p == NULL) return;
	if (x < p->data) { // look in left subtree
		removeST(p->left,x);
		return;
	} else if (p->data < x) {
		removeST(p->right,x);
		return;
	}
	// at this point we know x == p->data, so we've found the node
	// to remove.  Check which case we are in:
	if (p->left == NULL || p->right == NULL) {
		/* add pointers together, counting on NULL being
		 * 0 so that it is the identity element say for xor */
		node* child = (node*)((size_t)p->left ^ (size_t)p->right);
		delete p;
		p = child;
	} else { // annoying case of two children...
		node* s = p->right; /* will store successor */
		while (s->left) s = s->left;
		p->data = s->data;
		removeST(p->right,s->data);
		/* this recursive call will be caught by the previous case. */
	}
}

void tree::remove(int x)
{
	removeST(root,x);
}

/* traversal stuff: */

// typedef map<int,string>::iterator map_i;

typedef void (*nodeproc)(node*&);
/* interpretation: "nodeproc is a pointer to a function
 * that accepts a node pointer by reference and returns void" */
void printnode(node*& p) { printf("%i ",p->data); }
void deletenode(node*& p) { delete p; }


/* IDEA: apply f to each node in the subtree rooted at p,
 * in-order. */
void preorder(node*& p, nodeproc f)
{
	if (p == NULL) return;
	f(p);
	preorder(p->left,f);
	preorder(p->right,f);
}
void inorder(node*& p, nodeproc f)
{
	if (p == NULL) return;
	inorder(p->left,f);
	f(p); /* process root of subtree: */
	inorder(p->right,f);
}
void postorder(node*& p, nodeproc f)
{
	if (p == NULL) return;
	postorder(p->left,f);
	postorder(p->right,f);
	f(p);
}

void tree::printInorder()
{
	inorder(root,printnode);
	printf("\n");
}
void tree::printPreorder()
{
	preorder(root,printnode);
	printf("\n");
}
void tree::printPostorder()
{
	postorder(root,printnode);
	printf("\n");
}

/* another version which accounts for the possibility that the
 * function processing a node might need a parameter: */
typedef void (*nodeprocEx)(node*&,void*);
/* NOTE: we use void* as a sort of "universal pointer".  It could
 * point to anything, and must be used carefully.  There will be
 * a tacit contract between the caller and callee about what type
 * of thing the pointer points to!  See the printDot function below
 * where the pointer is interpreted as a file handle. */

/* And here is an overloaded postorder using the extended function
 * pointer type (used by writeDot below) */
void postorder(node*& p, nodeprocEx f, void* pParams)
{
	if (p == NULL) return;
	postorder(p->left,f,pParams);
	postorder(p->right,f,pParams);
	f(p,pParams);
}

/* TODO: using a traversal and the "extended" function pointer type
 * nodeprocEx, write something to add all values in a tree.
 * Hint if you need/want it: you'll want to write a function matching
 * the prototype of nodeprocEx which does a little work, and perhaps
 * uses that void* to store an accumulated value.  Then add a function
 * like tree::sum() which just gives your nodeprocEx to a traversal
 * and returns the result. */

/* TODO: make sure this destructor makes sense! */
tree::~tree()
{
	postorder(root,deletenode);
}

/* print corresponding parenthesis, as shown here:
 * https://en.wikipedia.org/wiki/Binary_tree#Combinatorics
 * */
void parens(node* p)
{
	if (p == NULL) return;
	printf("(");
	parens(p->left);
	printf(")");
	parens(p->right);
	/* NOTE: this doesn't quite fit into any of the traversals! */
}
void tree::printParens()
{
	parens(root);
	printf("\n");
}

/* for writing a dot file
 * https://en.wikipedia.org/wiki/Dot_language
 * */
void printDot(node*& p, void* pFILE)
{
	/* NOTE: the void* param is assumed to be a file stream which
	 * is opened for writing.  Also, you need to do this post order. */
	if (p==0) return;
	FILE* f = (FILE*)(pFILE);
	fprintf(f, "  \"%p\" [label=%i]\n", p,p->data);
	if (!(p->left||p->right)) return;
	/* nodes with a single child may be rendered directly below, so
	 * print invisible nodes to space it out: */
	if (!p->left) {
		fprintf(f, "  \"%s%p\" [label=\"\",width=.1,style=invis]","l",p);
		fprintf(f, "  \"%p\" -> \"%s%p\" [style=invis]\n",p,"l",p);
	} else {
		fprintf(f, "  \"%p\" -> \"%p\" [style=dashed]\n",p,p->left);
	}
	if (!p->right) {
		fprintf(f, "  \"%s%p\" [label=\"\",width=.1,style=invis]","r",p);
		fprintf(f, "  \"%p\" -> \"%s%p\" [style=invis]\n",p,"r",p);
	} else {
		fprintf(f, "  \"%p\" -> \"%p\"\n",p,p->right);
	}
}

void tree::writeDot(const char* fname)
{
	FILE* fdot = fopen(fname,"wb");
	fprintf(fdot, "digraph BST {\n");
	fprintf(fdot, "  graph [ordering=\"out\"];\n");
	fprintf(fdot, "  bgcolor=black\n  edge [color=white]\n");
	fprintf(fdot, "  node [style=filled color=white fillcolor=violet shape=circle]\n");
	postorder(root,&printDot,fdot);
	fprintf(fdot, "}\n");
	fclose(fdot);
}
