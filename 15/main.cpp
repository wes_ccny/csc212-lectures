#include "tree.h"
#include <iostream>
using std::cin;
using std::cout;

int main()
{
	int x;
	tree T;
	while (cin >> x) {
		T.insert(x);
	}
	/* TODO: test out your sum() function. */
	T.printInorder();
	T.printPreorder();
	T.printPostorder();
	T.printParens();
	T.writeDot("/tmp/tree0.dot");
	T.remove(10);
	T.writeDot("/tmp/tree1.dot");
	/* NOTE: you can render tree.dot to tree.svg with the following:
	 * dot -Tsvg -o tree.svg tree.dot
	 * (Provided that you have graphviz, which is installed in the 103
	 * virtual machine, btw.) */
	return 0;
}
