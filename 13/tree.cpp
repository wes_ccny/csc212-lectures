#include "tree.h"

/* TODO: try to fill this out a bit more by writing:
 * - generic traversal functions (using function pointers;
 *   ask me how if you don't know about them)
 * - copy constructor
 * - remove function
 * - */

template <typename T>
void swap(T& a, T& b)
{
	T t = a;
	a = b;
	b = t;
}

tree::tree()
{
	root = NULL;
}

// A = B; --> A.operator=(B);
tree& tree::operator=(tree T)
{
	/* assuming a valid copy constructor... */
	swap(root,T.root);
	return *this;
} // T goes out of scope and cleans up our old data...


bool searchST(node* p, int x)
{
	if (!p) return false; /* empty subtree */
	if (x < p->data) return searchST(p->left,x);
	if (x > p->data) return searchST(p->right,x);
	return true;
}
bool tree::search(int x)
{
	return searchST(root,x);
}


/* insert as leaf node */
void insertST(node*& p, int x)
{
	if (!p) {
		p = new node(x);
		return;
	}
	if (x < p->data) insertST(p->left,x);
	else if (x > p->data) insertST(p->right,x);
	/* otherwise x is a duplicate (==p->data) */
}
void tree::insert(int x)
{
	return insertST(root,x);
}
