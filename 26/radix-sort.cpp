#include <vector>
using std::vector;
#include <iostream>
using std::cin;
using std::cout;

/* sort A using the d-th digit in base 2^b of
 * each integer as the sort key.  Further, let's
 * say that b divides the bit-length of each integer.
 * (Bit-length == number of digits in a base 2
 * represenation.) */
void countingsort(vector<size_t>& A, size_t d, size_t b)
{
	/* mask should be 000000's followed by b 1's: */
	size_t mask = (size_t)(-1)>>(sizeof(size_t)*8-b);
	/* first make a frequency table: */
	vector<size_t> C(1<<b,0);
	for (size_t i = 0; i < A.size(); i++)
		C[(A[i]>>(d*b))&mask]++;
	/* now modify C so that C[i] == #values <= i in A: */
	for (size_t i = 1; i < C.size(); i++)
		C[i] += C[i-1];
	vector<size_t> R(A.size());
	/* now fill R with the sorted result: */
	for (size_t i = A.size()-1; i != (size_t)(-1); i--) {
		/* look in C[digit d of A[i]].  Place A[i]
		 * at this location in R and decrement C for
		 * next time. */
		R[--C[(A[i]>>(d*b))&mask]] = A[i];
	}
	/* at this point result is in R */
	A = std::move(R);
	/* NOTE: std::move will sort of cast R to an r-value
	 * so that we can make use of the more efficient r-value
	 * assignment if it exists. */
}

/* "What I cannot create, I do not understand."
 *   -- Richard Feynman
 * TODO: Delete all this and re-write it! */

void radixsort(vector<size_t>& A)
{
	/* TODO: run timing tests and see if 16 bits is better or worse
	 * than 8 for varying sizes of the input.  (Due to CPU cache
	 * sizes, you might see some interesting things happen if you choose
	 * the base to be too large, even though it should theoretically
	 * perform quite well.) */
	size_t bitsofbase = 8;
	size_t digits = (sizeof(size_t)*8)/bitsofbase;
	/* it'll be 4 if bitsofbase is 16 */
	for (size_t i = 0; i < digits; i++)
		countingsort(A,i,bitsofbase);
	/* TODO: how would you modify this so it works even if number of
	 * bits of the base does not divide the machine's register size? */
}

int main()
{
	size_t x;
	vector<size_t> A;
	while (cin >> x) {
		A.push_back(x);
	}
	radixsort(A);
	for (size_t i = 0; i < A.size(); i++) {
		printf("%lu\n",A[i]);
	}
	return 0;
}
